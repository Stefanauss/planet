<?php

require_once ('../funzioni.php');
lugheader ('Calendario LUG', 'Eventi dei Linux Users Groups italiani');

?>

<div class="container main-contents">
	<h2>Questa pagina non esiste più!</h2>
	<h4>Ora il <a href="/eventi">calendario eventi</a> include anche altre comunità oltre ai LUG quindi è stato spostato!</h4>
</div>

<div style="clear: both; margin-bottom: 20px"></div>

<?php lugfooter (); ?>

