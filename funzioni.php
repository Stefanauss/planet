<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL ^ E_DEPRECATED);

function get_redirect($url) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_NOBODY, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT,10);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0');
    $headers = curl_exec($ch);
    curl_close($ch);
        
    if (strpos($headers,'403 Forbidden') !== false) {
        return str_replace('http://', 'https://', $url);
    }
    
    if (strpos($headers,'503 Service Unavailable') !== false) {
        return false;
    }

    if (preg_match('/^Location: (.+)$/im', $headers, $matches)) {
        if (strpos($matches[1],'http') === false) {
            return $url . $matches[1];
        }
        
        return trim($matches[1]);
    }
        
    return $url;
}

require_once('vendor/autoload.php');
require_once('config.php');
require_once('src/layout.php');
require_once('src/rss.php');
require_once('src/calendar.php');
